import os


def read_matrix_vectors(file_name, separator=" "):
    with open(file_name) as f:
        for line in f.readlines():
            vector = [int(x) for x in line.strip().split(separator)]
            if len(vector) == 0:
                return
            yield vector


def write_matrix_vectors(vectors, file_name, separator=" "):
    with open(file_name, "w") as f:
        for v in vectors:
            line = separator.join(str(x) for x in v) + "\n"
            f.write(line)


def make_zero_matrix(rows, cols):
    return [[0 for i in range(cols)] for j in range(rows)]


class FileBackedObjectList(object):
    def __init__(self, path_pattern):
        self.path_pattern = path_pattern
        self.last_read_key = None
        self.last_read_value = None
        
    def __getitem__(self, key):
        if key == self.last_read_key:
            return self.last_read_value
        self.last_read_value = self.read_from_file(self.path_pattern % key)
        self.last_read_key = key
        return self.last_read_value

    def __setitem__(self, key, value):
        if key == self.last_read_key:
            self.last_read_value = value
        self.write_to_file(self.path_pattern % key, value)
        
    def __contains__(self, key):
        file_path = self.path_pattern % key
        if not os.path.isfile(file_path):
            return False
        return os.path.getsize(file_path) != 0
    
    def read_from_file(self, file_path):
        pass
    
    def write_to_file(self, file_path, value):
        pass


class MatrixList(FileBackedObjectList):
    def __init__(self, path_pattern, separator=" ", lazy_read=False):
        super(MatrixList, self).__init__(path_pattern)
        self.separator = separator
        self.lazy_read = lazy_read

    def read_from_file(self, file_path):
        matrix = read_matrix_vectors(file_path, self.separator)
        if self.lazy_read:
            return matrix
        return list(matrix)

    def write_to_file(self, file_path, value):
        write_matrix_vectors(value, file_path, self.separator)


def add_with_matrix(m1, m2, scalar=1):
    i = 0
    for v in m2:
        for j in range(len(v)):
            m1[i][j] += scalar*v[j]
        i += 1


def subtract_with_matrix(m1, m2, scalar=1):
    add_with_matrix(m1, m2, scalar=-scalar)


def multiply_with_scalar(m1, scalar=1):
    return [[x*scalar for x in v] for v in m1]
