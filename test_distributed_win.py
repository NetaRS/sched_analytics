import unittest
from distributed_win import compute_dist_only_throughput_ex, compute_throughput_ex
import json

BASE_CONF_PATH = "conf.json"


class MyTestCase(unittest.TestCase):
    def test_dist_ex(self):
        conf = json.load(open(BASE_CONF_PATH))

        conf["compute_epoch"] = 3
        conf["agg_interval"] = 3
        conf["agg_epoch_delay"] = 2
        conf["threshold"] = 0.0
        conf["window"] = 1
        conf["iterations"] = 1
        tps, total_tp = compute_throughput_ex(**conf)

    def test_dist_only_ex(self):
        conf = json.load(open(BASE_CONF_PATH))

        conf["threshold"] = -1
        conf["window"] = 1
        conf["iterations"] = 1
        tps, total_tp = compute_dist_only_throughput_ex(**conf)


if __name__ == '__main__':
    unittest.main()
