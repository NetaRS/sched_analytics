from os import path
import json
import argparse
import run_analysis
import csv
from collections import defaultdict
import matplotlib.pyplot as plt

'''''''''''
DCTCP = [0     0
10000 0.15
20000 0.2
30000 0.3
50000 0.4
80000 0.53
200000 0.6
1e+06 0.7
2e+06 0.8
5e+06 0.9
1e+07 0.97
3e+07 1]

VL2 = [0   0
180 0.1
216 0.2
560 0.3
900 0.4
1100 0.5
1870 0.6
3160 0.7
10000 0.8
400000 0.9
3.16e+06 0.95
1e+08 0.98
1e+09 1]

HULL = [0     0
5000 0.049940
10000 0.541151
20000 0.778391
40000 0.892970
80000 0.948308
160000 0.975034
320000 0.987942
640000 0.994177
1280000 0.997187
2560000 0.998642
5120000 0.999344
10240000 1
]
'''''''''''

def read_from_file(CDF_filename):
    X = []
    Y = []
    filename = str(CDF_filename)
    f = open(filename, 'r')
    
    for line in f.readlines():
        x, y = map(float, line.strip().split())
        X.append(x)
        Y.append(y)
    return X, Y


def prepare_for_plot(cdf_x, cdf_y):
    if cdf_y[0] == 0:
        cdf_x[0] = cdf_x[1] - 1
    
        

def main():
    
    DCTCP_X, DCTCP_Y = read_from_file("DCTCP_CDF")
    prepare_for_plot(DCTCP_X, DCTCP_Y)
    HULL_X, HULL_Y = read_from_file("HULL_CDF")
    prepare_for_plot(HULL_X, HULL_Y)
    VL2_X, VL2_Y = read_from_file("VL2_CDF")
    prepare_for_plot(VL2_X, VL2_Y)
    plt.plot(DCTCP_X, DCTCP_Y, label="pFabric")#, linestyle=":")
    plt.plot(HULL_X, HULL_Y , label="HULL")#, linestyle="_")
    plt.plot(VL2_X, VL2_Y, label="VL2")#, linestyle="-")
    plt.xlabel('Flow Size (bytes)')
    plt.ylabel('CDF')
    plt.legend()
    plt.xscale("log")
    plt.savefig("three distributions.pdf")
    plt.show()
    
if __name__ == "__main__":
    main()
