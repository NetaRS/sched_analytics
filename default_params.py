
AGG_INTERVALS = [20, 40, 80]
AGG_EPOCH_DELAYS = [0, 1, 2]
COMPUTE_EPOCHES = AGG_INTERVALS
WINDOWS = [1, 2]
THRESHOLDS = [0.1*i for i in range(11)]
ITERATIONS = [1,2,3]
FORCE_COMPUTE_EQUAL_AGG = True
DEGREES = [1,2,3,4]


def update_globals(agg_intervals=AGG_INTERVALS,
                   agg_epoch_delays=AGG_EPOCH_DELAYS,
                   compute_epoches=COMPUTE_EPOCHES,
                   windows=WINDOWS,
                   thresholds=THRESHOLDS,
                   iterations=ITERATIONS,
                   force_compute_equal_agg=FORCE_COMPUTE_EQUAL_AGG,
                   degrees=DEGREES,
                   max_degree=None,
                   **kwargs):
    global AGG_INTERVALS, AGG_EPOCH_DELAYS, COMPUTE_EPOCHES, WINDOWS, THRESHOLDS, ITERATIONS, FORCE_COMPUTE_EQUAL_AGG, DEGREES
    AGG_INTERVALS = agg_intervals
    AGG_EPOCH_DELAYS = agg_epoch_delays
    COMPUTE_EPOCHES = compute_epoches
    WINDOWS = windows
    THRESHOLDS = thresholds
    ITERATIONS = iterations
    FORCE_COMPUTE_EQUAL_AGG = force_compute_equal_agg
    if max_degree is not None:
        DEGREES = [max_degree]
    else:
        DEGREES = degrees
